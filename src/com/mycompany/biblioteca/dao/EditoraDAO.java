package com.mycompany.biblioteca.dao;

import com.mycompany.biblioteca.dao.conexao.Conexao;
import com.mycompany.biblioteca.model.Editora;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class EditoraDAO {

    public void inserirEditora(Editora ed) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "INSERT INTO editora(cnpjEditora, nomeEditora) VALUES (?,?)";

        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, ed.getCnpjEditora());
        comando.setString(2, ed.getNomeEditora());

        comando.executeUpdate();
    }

    public ArrayList<Editora> listarEditoras() throws Exception {
        ArrayList<Editora> editoras = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM editora ORDER BY nomeEditora ASC";
        PreparedStatement comando = con.prepareStatement(SQL);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Editora ed = new Editora();

            ed.setCnpjEditora(rs.getString("cnpjEditora"));
            ed.setNomeEditora(rs.getString("nomeEditora"));

            editoras.add(ed);
        }
        return editoras;
    }

    public Editora consulta(String cnpj) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM editora WHERE cnpjEditora = ?";
        PreparedStatement ps = con.prepareStatement(SQL);
        ps.setString(1, cnpj);

        ResultSet comando = ps.executeQuery();

        if (comando.next()) {
            Editora editora = new Editora();

            editora.setCnpjEditora(comando.getString("cnpjEditora"));
            editora.setNomeEditora(comando.getString("nomeEditora"));

            return editora;
        }
        return null;
    }

    public boolean atualizarDados(Editora ed) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "UPDATE editora SET nomeEditora = ? WHERE cnpjEditora = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, ed.getNomeEditora());
        comando.setString(2, ed.getCnpjEditora());

        return (comando.executeUpdate() > 0);
    }

    public boolean removerEditora(String cnpjEditora) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "DELETE FROM editora WHERE cnpjEditora = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, cnpjEditora);

        return (comando.executeUpdate() > 0);
    }
}
