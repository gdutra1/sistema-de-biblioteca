package com.mycompany.biblioteca.dao;

import com.mycompany.biblioteca.dao.conexao.Conexao;
import com.mycompany.biblioteca.model.Escritor;
import java.sql.*;
import java.util.ArrayList;

public class EscritorDAO {

    public void inserirEscritor(Escritor es) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "INSERT INTO escritor(cdEscritor, nomeEscritor) VALUES (?, ?)";

        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setInt(1, es.getCdEscritor());
        comando.setString(2, es.getNomeEscritor());

        comando.executeUpdate();
    }

    public ArrayList<Escritor> listarEscritores() throws Exception {
        ArrayList<Escritor> escritores = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM escritor ORDER BY nomeEscritor ASC";
        PreparedStatement comando = con.prepareStatement(SQL);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Escritor escritor = new Escritor();

            escritor.setCdEscritor(rs.getInt("cdEscritor"));
            escritor.setNomeEscritor(rs.getString("nomeEscritor"));

            escritores.add(escritor);
        }

        return escritores;
    }

    public Escritor consulta(int codigo) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM escritor WHERE cdEscritor = ?";
        PreparedStatement comando = con.prepareStatement(SQL);
        comando.setInt(1, codigo);

        ResultSet rs = comando.executeQuery();

        if (rs.next()) {
            Escritor escritor = new Escritor();

            escritor.setCdEscritor(rs.getInt("cdEscritor"));
            escritor.setNomeEscritor(rs.getString("nomeEscritor"));

            return escritor;
        }
        return null;
    }

    public boolean atualizarDados(Escritor es) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "UPDATE escritor SET nomeEscritor = ? WHERE cdEscritor = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, es.getNomeEscritor());
        comando.setInt(2, es.getCdEscritor());

        return (comando.executeUpdate() > 0);     
    }

    public boolean removerEscritor(int cdEscritor) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "DELETE FROM escritor WHERE cdEscritor = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setInt(1, cdEscritor);

        return (comando.executeUpdate() > 0);
    }
}
