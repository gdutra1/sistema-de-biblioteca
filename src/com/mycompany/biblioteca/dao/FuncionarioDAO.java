package com.mycompany.biblioteca.dao;

import com.mycompany.biblioteca.dao.conexao.Conexao;
import com.mycompany.biblioteca.model.Funcionario;
import java.sql.*;
import java.util.ArrayList;

public class FuncionarioDAO {

    public void inserirFuncionario(Funcionario f) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "INSERT INTO funcionario(cpfFuncionario, nomeFuncionario) VALUES (?, ?)";

        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, f.getCpf());
        comando.setString(2, f.getNome());

        comando.executeUpdate();

    }

    public ArrayList<Funcionario> listarFuncionarios() throws Exception {
        ArrayList<Funcionario> funcionarios = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM funcionario ORDER BY nomeFuncionario ASC";
        PreparedStatement comando = con.prepareStatement(SQL);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            Funcionario f = new Funcionario();

            f.setCpf(rs.getString("cpfFuncionario"));
            f.setNome(rs.getString("nomeFuncionario"));

            funcionarios.add(f);
        }
        return funcionarios;
    }

    public Funcionario consulta(String cpf) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM funcionario WHERE cpfFuncionario = ?";
        PreparedStatement comando = con.prepareStatement(SQL);
        comando.setString(1, cpf);

        ResultSet rs = comando.executeQuery();

        if (rs.next()) {
            Funcionario funcionario = new Funcionario();

            funcionario.setCpf(rs.getString("cpfFuncionario"));
            funcionario.setNome(rs.getString("nomeFuncionario"));

            return funcionario;
        }

        return null;
    }

    public boolean atualizarDados(Funcionario f) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "UPDATE funcionario SET nomeFuncionario = ? WHERE cpfFuncionario = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, f.getNome());
        comando.setString(2, f.getCpf());

        return (comando.executeUpdate() > 0);
    }

    public boolean removerFuncionario(String cpfFuncionario) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "DELETE FROM funcionario WHERE cpfFuncionario = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, cpfFuncionario);

        return (comando.executeUpdate() > 0);
    }
}
