package com.mycompany.biblioteca.dao;

import com.mycompany.biblioteca.dao.conexao.Conexao;
import com.mycompany.biblioteca.model.Livro;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class LivroDAO {

    public void inserirLivro(Livro livro) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "INSERT INTO livro(isbn, titulo, unidadesPossuidas, "
                + "unidadesDisponiveis, editora, escritor) "
                + "VALUES (?, ?, ?, ?, ?, ?)";

        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, livro.getIsbn());
        comando.setString(2, livro.getTitulo());
        comando.setInt(3, livro.getUnidadesPossuidas());
        comando.setInt(4, livro.getUnidadesDisponiveis());
        comando.setString(5, livro.getEditora().getCnpjEditora());
        comando.setInt(6, livro.getEscritor().getCdEscritor());

        comando.executeUpdate();

    }

    public ArrayList<Livro> listarLivros() throws Exception {
        ArrayList<Livro> livros = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM livro ORDER BY titulo ASC";
        PreparedStatement comando = con.prepareStatement(SQL);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            livros.add(definirDados(new Livro(), rs));
        }
        return livros;
    }

    public Livro consulta(String isbn) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM livro WHERE isbn = ?";
        PreparedStatement comando = con.prepareStatement(SQL);
        comando.setString(1, isbn);

        ResultSet rs = comando.executeQuery();

        if (rs.next()) {
            return definirDados(new Livro(), rs);
        }
        return null;
    }

    public boolean atualizarDados(Livro livro) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "UPDATE livro SET titulo = ?, "
                + "unidadesPossuidas = ?, unidadesDisponiveis = ?, "
                + "editora = ?, escritor = ?"
                + "WHERE isbn = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, livro.getTitulo());
        comando.setInt(2, livro.getUnidadesPossuidas());
        comando.setInt(3, livro.getUnidadesDisponiveis());
        comando.setString(4, livro.getEditora().getCnpjEditora());
        comando.setInt(5, livro.getEscritor().getCdEscritor());
        comando.setString(6, livro.getIsbn());

        return (comando.executeUpdate() > 0);
    }

    public boolean removerLivro(String isbn) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "DELETE FROM livro WHERE isbn = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, isbn);

        return (comando.executeUpdate() > 0);

    }

    private Livro definirDados(Livro livro, ResultSet rs) throws Exception {
        livro.setIsbn(rs.getString("isbn"));
        livro.setTitulo(rs.getString("titulo"));
        livro.setUnidadesPossuidas(rs.getInt("unidadesPossuidas"));
        livro.setUnidadesDisponiveis(rs.getInt("unidadesDisponiveis"));
        livro.setEditora(new EditoraDAO().consulta(rs.getString("editora")));
        livro.setEscritor(new EscritorDAO().consulta(rs.getInt("escritor")));

        return livro;
    }
}
