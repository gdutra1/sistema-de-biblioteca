package com.mycompany.biblioteca.dao;

import com.mycompany.biblioteca.dao.conexao.Conexao;
import com.mycompany.biblioteca.model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UsuarioDAO {

    public void inserirUsuario(Usuario u) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "INSERT INTO usuario(cpfUsuario, nomeUsuario, "
                + "email, telefone, dataNascimento) "
                + "VALUES (?, ?, ?, ?, ?)";

        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, u.getCpf());
        comando.setString(2, u.getNome());
        comando.setString(3, u.getEmail());
        comando.setString(4, u.getTelefone());
        comando.setDate(5, u.getDataNascimento());

        comando.executeUpdate();

    }

    public ArrayList<Usuario> listarUsuarios() throws Exception {
        ArrayList<Usuario> usuarios = new ArrayList<>();

        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM usuario ORDER BY nomeUsuario";
        PreparedStatement comando = con.prepareStatement(SQL);

        ResultSet rs = comando.executeQuery();

        while (rs.next()) {
            usuarios.add(definirDados(new Usuario(), rs));
        }

        return usuarios;
    }

    public Usuario consulta(String cpf) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "SELECT * FROM usuario WHERE cpfUsuario = ?";
        PreparedStatement comando = con.prepareStatement(SQL);
        comando.setString(1, cpf);

        ResultSet rs = comando.executeQuery();

        if (rs.next()) {
            return definirDados(new Usuario(), rs);
        }
        return null;
    }

    public boolean atualizarDados(Usuario u) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "UPDATE usuario SET nomeUsuario = ?, "
                + "email = ?, telefone = ?, dataNascimento = ?"
                + "WHERE cpfUsuario = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, u.getNome());
        comando.setString(2, u.getEmail());
        comando.setString(3, u.getTelefone());
        comando.setDate(4, u.getDataNascimento());
        comando.setString(5, u.getCpf());

        return (comando.executeUpdate() > 0);
    }

    public boolean removerUsuario(String cpfUsuario) throws Exception {
        Connection con = Conexao.getConexao();
        String SQL = "DELETE FROM usuario WHERE cpfUsuario = ?";
        PreparedStatement comando = con.prepareStatement(SQL);

        comando.setString(1, cpfUsuario);

        return (comando.executeUpdate() > 0);
    }

    private Usuario definirDados(Usuario usuario, ResultSet rs) throws Exception {
        usuario.setCpf(rs.getString("cpfUsuario"));
        usuario.setNome(rs.getString("nomeUsuario"));
        usuario.setEmail(rs.getString("email"));
        usuario.setTelefone(rs.getString("telefone"));
        usuario.setDataNascimento(rs.getDate("dataNascimento"));

        return usuario;
    }
}
