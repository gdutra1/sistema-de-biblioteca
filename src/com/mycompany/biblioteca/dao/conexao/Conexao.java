package com.mycompany.biblioteca.dao.conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

    private static String url = "jdbc:postgresql://localhost/Biblioteca";
    private static String usuario = "";
    private static String senha = "";

    private static Connection conexao = null;

    public static Connection getConexao() {
        try {
            Conexao.conexao = DriverManager.getConnection(url, usuario, senha);
        } catch (Exception e) {
            Conexao.conexao = null;
        }
        return Conexao.conexao;
    }

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        Conexao.url = url;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        Conexao.usuario = usuario;
    }

    public static String getSenha() {
        return senha;
    }

    public static void setSenha(String senha) {
        Conexao.senha = senha;
    }

}
