package com.mycompany.biblioteca.model;

import java.util.Objects;

public class Editora {

    private String cnpjEditora;
    private String nomeEditora;
    
    public Editora(){}
    
    public Editora(String cnpj, String nomeEditora) {
        this.cnpjEditora = cnpj;
        this.nomeEditora = nomeEditora;
    }

    public String getCnpjEditora() {
        return cnpjEditora;
    }

    public void setCnpjEditora(String cnpjEditora) {
        this.cnpjEditora = cnpjEditora;
    }

    public String getNomeEditora() {
        return nomeEditora;
    }

    public void setNomeEditora(String nomeEditora) {
        this.nomeEditora = nomeEditora;
    }

    @Override
    public String toString() {
        return "Editora: " + nomeEditora + " | CNPJ: " + this.cnpjEditora;
    }

}
