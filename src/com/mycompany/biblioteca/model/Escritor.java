package com.mycompany.biblioteca.model;

public class Escritor {
    
    private int cdEscritor;
    private String nomeEscritor;

    public Escritor() {
    }

    public Escritor(int cdEscritor, String nomeEscritor) {
        this.cdEscritor = cdEscritor;
        this.nomeEscritor = nomeEscritor;
    }

    public int getCdEscritor() {
        return cdEscritor;
    }

    public void setCdEscritor(int cdEscritor) {
        this.cdEscritor = cdEscritor;
    }

    public String getNomeEscritor() {
        return nomeEscritor;
    }

    public void setNomeEscritor(String nomeEscritor) {
        this.nomeEscritor = nomeEscritor;
    }

    @Override
    public String toString() {
        return "Escritor: " + nomeEscritor + " | Código de identificação: " + cdEscritor;
    }
    
}
