package com.mycompany.biblioteca.model;

public class Funcionario extends Pessoa{

    public Funcionario(){}
    
    public Funcionario(String cpf, String nome) {
        super(cpf, nome);
    }

    @Override
    public String toString() {
        return "Funcionário: " + this.getNome() + " | CPF: " + this.getCpf();
    }
    
}
