package com.mycompany.biblioteca.model;

import java.sql.Date;

public class Livro {
    
    private String isbn;
    private String titulo;
    private int unidadesPossuidas;
    private int unidadesDisponiveis;
    private Editora editora;
    private Escritor escritor;

    public Livro() {
    }

    public Livro(String isbn, String titulo, int unidadesPossuidas, int unidadesDisponiveis, Editora editora, Escritor escritor) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.unidadesPossuidas = unidadesPossuidas;
        this.unidadesDisponiveis = unidadesDisponiveis;
        this.editora = editora;
        this.escritor = escritor;
    }
    
    public Livro(String isbn, String titulo, int unidadesPossuidas, Editora editora, Escritor escritor) {
        this.isbn = isbn;
        this.titulo = titulo;
        this.unidadesPossuidas = unidadesPossuidas;
        this.unidadesDisponiveis = unidadesPossuidas;
        this.editora = editora;
        this.escritor = escritor;
    }
    
    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getUnidadesPossuidas() {
        return unidadesPossuidas;
    }

    public void setUnidadesPossuidas(int unidadesPossuidas) {
        this.unidadesPossuidas = unidadesPossuidas;
    }

    public int getUnidadesDisponiveis() {
        return unidadesDisponiveis;
    }

    public void setUnidadesDisponiveis(int unidadesDisponiveis) {
        this.unidadesDisponiveis = unidadesDisponiveis;
    }

    public Editora getEditora() {
        return editora;
    }

    public void setEditora(Editora editora) {
        this.editora = editora;
    }

    public Escritor getEscritor() {
        return escritor;
    }

    public void setEscritor(Escritor escritor) {
        this.escritor = escritor;
    }

    @Override
    public String toString() {
        return "Livro:  " + titulo + " | " +
               "ISBN: " + isbn + " | " +
               "Possuídos: " + unidadesPossuidas + " | " +
               "Disponíveis: " + unidadesDisponiveis + " | " +
               "Escritor: " + escritor.getNomeEscritor() + " | " +
               "Editora: " + editora.getNomeEditora();
    }
    
}
