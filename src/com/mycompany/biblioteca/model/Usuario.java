package com.mycompany.biblioteca.model;

import java.sql.*;
import java.text.SimpleDateFormat;

public class Usuario extends Pessoa{
    
    private String email;
    private String telefone;
    private Date dataNascimento;
            
    public Usuario(){}
    
    public Usuario(String cpf, String nome, String email, String telefone, Date dataNascimento) {
        super(cpf, nome);
        this.email = email;
        this.telefone = telefone;
        this.dataNascimento = dataNascimento;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public Date getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(Date dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        return "Usuário: " + this.getNome() + " | " +
               "CPF: " + this.getCpf() + " | " + 
               "E-mail: " + email + " | " +
               "Telefone: " + telefone + " | " +
               "Data de Nascimento: " + sdf.format(dataNascimento);
    }

}
