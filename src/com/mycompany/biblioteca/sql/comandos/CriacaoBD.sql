--É necessário que cada tabela seja criada de forma individual
CREATE TABLE Funcionario(
cpfFuncionario varchar(14) primary key,
nomeFuncionario varchar(150)
);

CREATE TABLE Usuario(
cpfUsuario varchar(14) primary key,
nomeUsuario varchar(150),
dataNascimento date,
email varchar(50),
telefone varchar(20)
);

CREATE TABLE Editora(
cnpjEditora varchar(18) primary key,
nomeEditora varchar(100)
);

CREATE TABLE Escritor(
cdEscritor int primary key,
nomeEscritor varchar(150)
);

CREATE TABLE Livro(
isbn varchar(20) primary key,
titulo varchar(150),
unidadesPossuidas int,
unidadesDisponiveis int,
escritor int REFERENCES ESCRITOR,
editora varchar(18) REFERENCES EDITORA
);
