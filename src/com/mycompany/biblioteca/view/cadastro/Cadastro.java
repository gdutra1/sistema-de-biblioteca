package com.mycompany.biblioteca.view.cadastro;

import javax.swing.JDialog;

public abstract class Cadastro extends JDialog{
    
    protected int modo;
    public static final int MODO_INSERCAO = 0, MODO_EDICAO = 1;

    public Cadastro(java.awt.Frame parent, boolean modal){
        super(parent, modal);
        this.modo = MODO_INSERCAO;
    }
    
    protected void inicializar(){
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    protected abstract void atualizarCampos();
    
    public void mostrar(int modo){
        this.setModo(modo);
        atualizarCampos();
        
        setLocationRelativeTo(null);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        
        setVisible(true);
    }
    
    public void mostrar(){
        mostrar(this.modo);
    }
      
    public int getModo() {
        return modo;
    }

    public void setModo(int modo) {
        this.modo = modo;
    }
    
}
