package com.mycompany.biblioteca.view.listagem;

import com.mycompany.biblioteca.dao.EditoraDAO;
import com.mycompany.biblioteca.model.Editora;
import com.mycompany.biblioteca.view.cadastro.Cadastro;
import com.mycompany.biblioteca.view.cadastro.CadastroEditora;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class ListagemEditora extends Listagem {

    private EditoraDAO editoraDAO = new EditoraDAO();
    private CadastroEditora cadastro = new CadastroEditora(null, true);

    public ListagemEditora() {
        super();
    }

    @Override
    public void listar() {
        DefaultListModel dlm = new DefaultListModel();
        try {
            dlm.addAll(editoraDAO.listarEditoras());
            atualizarList(dlm);
        } catch (Exception e) {
            String mensagem = "Um erro ocorreu ao tentar listar as editoras.";
            JOptionPane.showMessageDialog(this.getParent(), mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void adicionar() {
        cadastro.mostrar(CadastroEditora.MODO_INSERCAO);
        listar();
    }
    
    @Override
    public void remover() {
        Editora editora = (Editora) getObjetoSelecionado();

        if (editora != null) {
            String mensagem = "Tem certeza que deseja remover a editora \""
                    + editora.getNomeEditora() + "\" ?";
            int opcao = JOptionPane.showConfirmDialog(this.getParent(), mensagem, "Remover?", JOptionPane.OK_CANCEL_OPTION);

            if (opcao == JOptionPane.OK_OPTION) {
                try {
                    editoraDAO.removerEditora(editora.getCnpjEditora());
                    listar();
                } catch (Exception ex) {
                    String mensagemErro = "Ocorreu um erro ao remover a editora.";
                    JOptionPane.showMessageDialog(this.getParent(), mensagemErro, "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    @Override
    public void editar() {
        Editora editora = (Editora) getObjetoSelecionado();

        if (editora != null) {
            cadastro.setEditora(editora);
            cadastro.mostrar(CadastroEditora.MODO_EDICAO);
            listar();
        }
    }

}
