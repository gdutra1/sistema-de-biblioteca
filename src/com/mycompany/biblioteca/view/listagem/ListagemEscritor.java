package com.mycompany.biblioteca.view.listagem;

import com.mycompany.biblioteca.dao.EscritorDAO;
import com.mycompany.biblioteca.model.Escritor;
import com.mycompany.biblioteca.view.cadastro.Cadastro;
import com.mycompany.biblioteca.view.cadastro.CadastroEscritor;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class ListagemEscritor extends Listagem{
    
    private EscritorDAO escritorDAO = new EscritorDAO();
    private CadastroEscritor cadastro = new CadastroEscritor(null, true);

    @Override
    public void listar() {
        DefaultListModel dlm = new DefaultListModel();
        try {
            dlm.addAll(escritorDAO.listarEscritores());
            atualizarList(dlm);
        } catch (Exception e) {
            String mensagem = "Um erro ocorreu ao tentar listar os escritores.";
            JOptionPane.showMessageDialog(this.getParent(), mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void adicionar() {
        cadastro.mostrar(Cadastro.MODO_INSERCAO);
        listar();
    }

    @Override
    public void remover() {
        Escritor escritor = (Escritor) getObjetoSelecionado();

        if (escritor != null) {
            String mensagem = "Tem certeza que deseja remover o escritor \""
                    + escritor.getNomeEscritor()+ "\" ?";
            int opcao = JOptionPane.showConfirmDialog(this.getParent(), mensagem, "Remover?", JOptionPane.OK_CANCEL_OPTION);

            if (opcao == JOptionPane.OK_OPTION) {
                try {
                    escritorDAO.removerEscritor(escritor.getCdEscritor());
                    listar();
                } catch (Exception ex) {
                    String mensagemErro = "Ocorreu um erro ao remover o escritor.";
                    JOptionPane.showMessageDialog(this.getParent(), mensagemErro, "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    @Override
    public void editar() {
        Escritor escritor = (Escritor) getObjetoSelecionado();

        if (escritor != null) {
            cadastro.setEscritor(escritor);
            cadastro.mostrar(CadastroEscritor.MODO_EDICAO);
            listar();
        }
    }
    
}
