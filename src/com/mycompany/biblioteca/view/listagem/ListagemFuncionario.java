package com.mycompany.biblioteca.view.listagem;

import com.mycompany.biblioteca.dao.FuncionarioDAO;
import com.mycompany.biblioteca.model.Funcionario;
import com.mycompany.biblioteca.view.cadastro.Cadastro;
import com.mycompany.biblioteca.view.cadastro.CadastroFuncionario;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class ListagemFuncionario extends Listagem{

    private FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
    private CadastroFuncionario cadastro  = new CadastroFuncionario(null, true);
    
    @Override
    public void listar() {
        DefaultListModel dlm = new DefaultListModel();
        try {
            dlm.addAll(funcionarioDAO.listarFuncionarios());
            atualizarList(dlm);
        } catch (Exception e) {
            String mensagem = "Um erro ocorreu ao tentar listar os funcionários.";
            JOptionPane.showMessageDialog(this.getParent(), mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void adicionar() {
        cadastro.mostrar(Cadastro.MODO_INSERCAO);
        listar();
    }

    @Override
    public void remover() {
        Funcionario funcionario = (Funcionario) getObjetoSelecionado();

        if (funcionario != null) {
            String mensagem = "Tem certeza que deseja remover o funcionário \""
                    + funcionario.getNome()+ "\" ?";
            int opcao = JOptionPane.showConfirmDialog(this.getParent(), mensagem, "Remover?", JOptionPane.OK_CANCEL_OPTION);

            if (opcao == JOptionPane.OK_OPTION) {
                try {
                    funcionarioDAO.removerFuncionario(funcionario.getCpf());
                    listar();
                } catch (Exception ex) {
                    String mensagemErro = "Ocorreu um erro ao remover o funcionário.";
                    JOptionPane.showMessageDialog(this.getParent(), mensagemErro, "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    @Override
    public void editar() {
        Funcionario funcionario = (Funcionario) getObjetoSelecionado();

        if (funcionario != null) {
            cadastro.setFuncionario(funcionario);
            cadastro.mostrar(Cadastro.MODO_EDICAO);
            listar();
        }
    }
    
}
