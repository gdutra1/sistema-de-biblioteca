package com.mycompany.biblioteca.view.listagem;

import com.mycompany.biblioteca.dao.LivroDAO;
import com.mycompany.biblioteca.model.Livro;
import com.mycompany.biblioteca.view.cadastro.Cadastro;
import com.mycompany.biblioteca.view.cadastro.CadastroEscritor;
import com.mycompany.biblioteca.view.cadastro.CadastroLivro;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class ListagemLivro extends Listagem{

    private LivroDAO livroDAO = new LivroDAO();
    private CadastroLivro cadastro = new CadastroLivro(null, true);
    
    @Override
    public void listar() {
        DefaultListModel dlm = new DefaultListModel();
        try {
            dlm.addAll(livroDAO.listarLivros());
            atualizarList(dlm);
        } catch (Exception e) {
            String mensagem = "Um erro ocorreu ao tentar listar os livros.";
            JOptionPane.showMessageDialog(this.getParent(), mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void adicionar() {
        cadastro.mostrar(Cadastro.MODO_INSERCAO);
        listar();
    }

    @Override
    public void remover() {
        Livro livro = (Livro) getObjetoSelecionado();

        if (livro != null) {
            String mensagem = "Tem certeza que deseja remover o livro \""
                    + livro.getTitulo()+ "\" ?";
            int opcao = JOptionPane.showConfirmDialog(this.getParent(), mensagem, "Remover?", JOptionPane.OK_CANCEL_OPTION);

            if (opcao == JOptionPane.OK_OPTION) {
                try {
                    livroDAO.removerLivro(livro.getIsbn());
                    listar();
                } catch (Exception ex) {
                    String mensagemErro = "Ocorreu um erro ao remover o livro.";
                    JOptionPane.showMessageDialog(this.getParent(), mensagemErro, "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    @Override
    public void editar() {
        Livro livro = (Livro) getObjetoSelecionado();

        if (livro != null) {
            cadastro.setLivro(livro);
            cadastro.mostrar(CadastroEscritor.MODO_EDICAO);
            listar();
        }
    }
    
}
