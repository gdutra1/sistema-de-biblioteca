package com.mycompany.biblioteca.view.listagem;

import com.mycompany.biblioteca.dao.UsuarioDAO;
import com.mycompany.biblioteca.model.Usuario;
import com.mycompany.biblioteca.view.cadastro.Cadastro;
import com.mycompany.biblioteca.view.cadastro.CadastroUsuario;
import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

public class ListagemUsuario extends Listagem{

    private UsuarioDAO usuarioDAO = new UsuarioDAO();
    private CadastroUsuario cadastro = new CadastroUsuario(null, true);
    
    @Override
    public void listar() {
        DefaultListModel dlm = new DefaultListModel();
        try {
            dlm.addAll(usuarioDAO.listarUsuarios());
            atualizarList(dlm);
        } catch (Exception e) {
            String mensagem = "Um erro ocorreu ao tentar listar os usuários.";
            JOptionPane.showMessageDialog(this.getParent(), mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @Override
    public void adicionar() {
        cadastro.mostrar(Cadastro.MODO_INSERCAO);
        listar();
    }

    @Override
    public void remover() {
        Usuario usuario = (Usuario) getObjetoSelecionado();

        if (usuario != null) {
            String mensagem = "Tem certeza que deseja remover o usuário \""
                    + usuario.getNome()+ "\" ?";
            int opcao = JOptionPane.showConfirmDialog(this.getParent(), mensagem, "Remover?", JOptionPane.OK_CANCEL_OPTION);

            if (opcao == JOptionPane.OK_OPTION) {
                try {
                    usuarioDAO.removerUsuario(usuario.getCpf());
                    listar();
                } catch (Exception ex) {
                    String mensagemErro = "Ocorreu um erro ao remover o livro.";
                    JOptionPane.showMessageDialog(this.getParent(), mensagemErro, "Erro", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    @Override
    public void editar() {
        Usuario usuario = (Usuario) getObjetoSelecionado();

        if (usuario != null) {
            cadastro.setUsuario(usuario);
            cadastro.mostrar(Cadastro.MODO_EDICAO);
            listar();
        }
    }
    
}
